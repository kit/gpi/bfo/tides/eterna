Eterna - Programs for tidal analysis and prediction
===================================================

Copyright (c) 1972, 1997 by Hans-Georg Wenzel  

See [CONTRIBUTORS](CONTRIBUTORS) for further contributors.

Contents are provided under the terms of the 
[GNU General Public License](LICENSE).

See file [INSTALL](INSTALL.md) for installation and usage instructions

Purpose and history
-------------------

Eterna, the 'Earth tide data processing package', was created by the late
Prof. Hans-Georg Wenzel at the Black Forest Observatory and the University of
Karlsruhe in the 1990s. This collection of programs established one standard
of tidal analysis and prediction in the Earth tide community. Originally
distributed only in the form of executable binaries, source code started to be
distributed in the community after Hans-Georg Wenzel passed away in 1999.

Access to source code is essential in order to keep the program running on new
computer platforms with recent compilers. Minor (and in a few cases major)
modifications where applied by researchers to correct errors or support new
features. This resulted in the unpleasant situation that different versions of
the programs are circulated, where the relation to the original version is
unclear (in particular with respect to source code modifications) and a clear
statement is missing what holders of the source are allowed to do or not.
Meanwhile (20 years after Eterna was first published) technical means are
commonplace to publically distribute source code together with proper
licensing and with each single modification being properly documented (such
that the original author must not be blamed for mistakes introduced later).

In the belief that it would be in the intention of Hans-Georg Wenzel to
support the continued use of Eterna in the tidal community, while allowing a
clear distinction between current modifications and his original version and
in the consent with his widow, Marion Wenzel, we publish the source code 
under terms of the GNU General Public License on a server of the Karlsruhe
Institute of Technology (KIT) which is the successor of the University of
Karlsruhe.

The original package came with many auxiliary programs for data pre-processing
and plot programs. A full set of supplementary data was included. The current
repository focuses on programs providing tidal prediction and tidal analysis
and does not contain the full content of the original CD-Rom version. Only
some selected auxiliary programs are included.

3rd party code
--------------

Hans-Georg Wenzel uses 3rd party code in his programs. There is nothing wrong
with this as long as the source code is not going to be published. Some of
these subroutines are not GPL compliant. They had to be replaced by GPL
compliant source code. See also the [CHANGELOG](CHANGELOG) and the comments in
the source code.

References
----------

Wenzel, H.-G. (1994): Earth tide data processing package ETERNA
   3.20. Bulletin d'Informations Marees Terrestres, vol. 120,
   9019-9022, Bruxelles 1994.
   http://www.bim-icet.org/ (last accessed 2019-05-22).

Wenzel, H.-G. (1995): Format and structure for the exchange of
   high precision tidal data. Bulletin d'Informations Marees
   Terrestres, vol. 121, 9097-9101, Bruxelles 1995.
   http://www.bim-icet.org/ (last accessed 2019-05-22).

Wenzel, H.-G. (1996): The nanogal software: Earth tide data
   processing package ETERNA 3.30. Bulletin d'Informations
   Marees Terrestres, vol. 124, 9425-9439, Bruxelles 1996.
   http://www.bim-icet.org/ (last accessed 2019-05-22).

Wenzel, H.-G. (2022): Eterna - Programs for tidal analysis and prediction. 
   Karlsruhe Institute of Technology. 
   DOI: [10.35097/746](http://dx.doi.org/10.35097/746)

See comments in the source code for further references.

See file [CONTRIBUTORS](CONTRIBUTORS) for acknowledgments.

Copyright, license, and disclaimer of warranty
----------------------------------------------

Eterna is the 'Earth tide data processing package' as created by the late
Hans-Georg Wenzel.
See source code for details.

Eterna - Programs for tidal analysis and prediction  
Copyright (C) 1972, 1997 by Hans-Georg Wenzel  

This collection of programs is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

These programs are distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](LICENSE) for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Home repository and caretaker
-----------------------------

The copy of this repository might be a clone or fork of the original one.  
The home of the public version of Eterna is 
https://gitlab.kit.edu/kit/gpi/bfo/tides/eterna

The git repository at KIT is maintained by

Thomas Forbriger  
email: Thomas.Forbriger@kit.edu  
Geophysical Institute, Karlsruhe Institute of Technology (KIT)  
Black Forest Observatory (BFO), Heubach 206, 77709 Wolfach, Germany,  
http://www.black-forest-observatory.de
