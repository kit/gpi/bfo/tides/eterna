#!/bin/gawk
# this is <etfiltconv.awk>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2012, 2015, 2020 by Thomas Forbriger (BFO Schiltach) 
# 
# convert eterna FIR filter coefficients to ASCII
#
# ----
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ----
#
# Use option "-v SCALE=1." if sample shall be scaled appropriately to provide
# actual filter response after Fourier transformation (understood as integral
# transformation) of the time series.
#
# Use option "-v HIGHPASS=1." to compute filter response for corresponding
# high-pass filter.
#
# Scaling must be done with T/2/dt, where T is the length of time series and
# T/2/dt=N/2=T*f_Ny, where N is the number of coefficients and f_Ny=1/(2*dt)
# is the Nyquist frequency
# 
# REVISIONS and CHANGES 
#    04/05/2012   V1.0   Thomas Forbriger
#    18/08/2015   V1.1   provide scaling
#    16/06/2020   V1.2   provide high-pass computation
#    17/06/2020   V1.3   fix scaling
# 
# ============================================================================
#
BEGIN { hot=0; i=0; DT=1.; FAC=1.; print "## etfiltconv.awk"; }
/^C\*\*\*/ { hot=1; next; }
/^DDT=/ { 
  if (hot) 
  { 
    print "# dt: " $2; DT=$2; 
  }
}
/^ / { if (hot) { v[i]=$2*FAC; ++i; } }
END { 
  if (SCALE > 0.) { FAC=(2.*i-1.)/2.; }
  if (HIGHPASS > 0.)
  {
    for (j=0; j<i-1;++j) { v[j] = 0.-v[j]; }
    v[i-1]=1.-v[i-1];
  }
  for (j=0; j<i;++j) { print FAC*v[j]; } 
  for (j=i-1;j>0;--j) { print FAC*v[j-1]; } 
}
# ----- END OF etfiltconv.awk ----- 
