#!/usr/bin/gnuplot
# this is <FIRspec.gpt.tpl>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2020 by Thomas Forbriger (BFO Schiltach) 
# 
# gnuplot template for FIRspec output
# 
# REVISIONS and CHANGES 
#    16/06/2020   V1.0   Thomas Forbriger
# 
# ============================================================================
#
set terminal postscript
set output '<FILTER>.FIR.ps'
set title 'amplitude response of zero-phase filter <FILTER>'
set xlabel 'frequency / f_{Ny}'
set grid
plot '<FILTER>.FIR.txt' u 1:2 w l t 'low-pass', \
     '<FILTER>.FIR.txt' u 1:3 w l t 'high-pass'
set logscale x
replot
set nologscale x
set logscale y
set yrange[1.e-10:1.1]
set ytics 1.e-10 10. logscale
plot '<FILTER>.FIR.txt' u ($1):(abs($2)) w l t 'low-pass', \
     '<FILTER>.FIR.txt' u ($1):(abs($3)) w l t 'high-pass'
set logscale x
replot
# ----- END OF FIRspec.gpt.tpl ----- 
