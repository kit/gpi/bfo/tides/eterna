#!/usr/bin/gnuplot
# this is <etfilter.gpt.tpl>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2020 by Thomas Forbriger (BFO Schiltach) 
# 
# gnuplot template for etfilter output
# 
# REVISIONS and CHANGES 
#    16/06/2020   V1.0   Thomas Forbriger
# 
# ============================================================================
#
set terminal postscript
set output '<FILTER>.resp.ps'
set title 'amplitude response of zero-phase filter <FILTER>'
set xlabel 'frequency / cpd'
set grid
plot '<FILTER>.fresp.txt' u 1:2 w l t 'low-pass', \
     '<FILTER>.fresp.txt' u 1:3 w l t 'high-pass'
# ----- END OF etfilter.gpt.tpl ----- 
