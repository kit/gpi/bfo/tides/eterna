c this is <FIRspec.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 2021 by Thomas Forbriger (KIT, GPI, BFO) 
c
c Compute spectral coefficients of FIR filter sequence.
c
c This program computes the filter response with frequency given as a
c fraction of the Nyquist frequency of the input series. The full
c frequency range from zero to Nyquist is computed. The number of
c frequency value can be selected.
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c REVISIONS and CHANGES
c    15/07/2021   V1.0   Thomas Forbriger
c
      program FIRspec
c
c name of file containing filter coefficients
        character filterfile*80
c name of output file
        character outputfile*80
c frequency range and frequency / cpd
        double precision fmin, fmax, f
c number of frequencies to compute, index
        integer nf, i
c 
c maximum number of filter coefficients
      integer mc
      parameter(mc=2001)
c actual number of filter coefficients
      integer nfi, nfi2
c array of filter coefficients
      double precision dfil(mc)
c name of filter
      character cfilt*12
c
c sampling interval / s
      double precision ddtsec
c 
c error flag
      integer nerr
c
c file unit for output
      integer luout
      parameter(luout=11)
c
c filter amplification (low-pass, high-pass)
      double precision flamp, fhamp
c
c ----------------------------------------------------------------------
c read user parameters
      print *,'enter path to file containing filter coefficients:'
      read (5, '(a80)') filterfile
      print *,'enter path to file for output of program:'
      read (5, '(a80)') outputfile
      print *,'enter number of frequencies:'
      read (5, *) nf
c 
      call ETLFIN(filterfile,mc,DDTSEC,NFI,nfi2,Dfil,CFILT,IERR)
      if (ierr.ne.0) then
        stop 'ERROR while reading filter parameters'
      endif
      print *,'filter is designed for input sampling interval of ',
     &  ddtsec,'s'
c
      print *,'open output file ',outputfile(1:index(outputfile,' ')-1)
      open(luout, file=outputfile, status='new', err=93)
      do i=1,nf
c compute frequency / cpd
        f=dble(i)/dble(nf)
        call famp(f, nfi, nfi2, dfil, flamp, fhamp)
        print *,'compute response at ',f,' * fNy'
        write (luout, fmt=100, err=95) f, flamp, fhamp
      enddo
      close(luout, err=94)
c 
      stop
  100 format(f20.15,2x,f20.15,2x,f20.15)
   95 stop 'ERROR: writing output file'
   94 stop 'ERROR: closing output file'
   93 stop 'ERROR: opening output file'
      end
c 
c ======================================================================
c
c return filter amplification for frequency f / fNy
c i.e. frequency is given as a fraction of Nyquist frequency
c 
      subroutine famp(f, nfi, nfi2, dfil, flamp, fhamp)
      integer nfi
      double precision retval, f, ddtsec, dfil(nfi)
      double precision flamp, fhamp
      integer msamp
      parameter (msamp=3000)
      double precision dstor(msamp)
      integer ia, ie, i
      double precision t,om
      if (nfi.gt.msamp) stop 'ERROR: dstor array size is to small'
      ia=1
      ie=nfi
c angular frequency in rad/s
      om=3.1415926535897931159d0*f
c center (origin of time)
      ic=IA+NFI2-1
      do i=ia,ie
        t=(i-ic)
        dstor(i)=dcos(t*om)
      enddo
      call ETFILT(NFI,NFI2,dfil,IA,IE,flamp,fhamp,msamp, dstor)
      return
      end
c 
c ======================================================================
C
      SUBROUTINE ETFILT(NFI,NFI2,DFIL,IA,IE,DFL,DFH,maxsto, dstor)
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c
c based on a copy of subroutine ETFILT from analyze.f
c
c IE-IA=NFI-1 by definition
c   IA: first sample in dstor to be used
c   IE: last sample in dstor to be used
c DSTOR(IA+NFI2-1): sample at time for which the filter result will be
c   returned
C                                                                      !
C     Routine ETFILT, version 1996.03.01 Fortran 90.                   !
C                                                                      !
C     The routine does numerical filtering of the data stored in       !
C     array DSTOR using a symmetrical numerical FIR lowpass filter.    !
C                                                                      !
C     All parameters with D as first character are DOUBLE PRECISION.   !
C                                                                      !
C     Input parameter description:                                     !
C     ----------------------------                                     !
C                                                                      !
C     NFI:         Number of filter coefficients (filter length).      !
C     NFI2:        Index of central filter coefficient.                !
C     DFIL:        Array of lowpass filter coefficients (1...NFI).     !
C                                                                      !
C     Output parameter description:                                    !
C     -----------------------------                                    !
C                                                                      !
C     DFL:         Lowpass  filtered observation vector (1:MAXNC).     !
C     DFH:         Highpass filtered observation vector (1:MAXNC).     !
C                                                                      !
C     COMMON /STORE/:                                                  !
C     --------------                                                   !
C                                                                      ! 
C     DSTOR:        Array(        1:MAXSTO), in which the Earth tide   !
C                   and meteorological observations are stored.        !
C                                                                      !
C     Used routines:  none.                                            !
C     --------------                                                   !
C                                                                      !
C     Routine creation:  1991.06.29 by Hans-Georg Wenzel,              !
C                        Black Forest Observatory,                     !
C                        Universitaet Karlsruhe,                       !
C                        Englerstr. 7,                                 !
C                        D-76128 KARLSRUHE,                            !
C                        Germany.                                      !
C                        Tel.: 0721-6082307,                           !
C                        FAX:  0721-694552.                            !
C                        e-mail: wenzel@gik.bau-verm.uni-karlsruhe.de  !
C     Last modification: 1996.03.01 by Hans-Georg Wenzel.              !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      IMPLICIT DOUBLE PRECISION (D)
      integer nfi, nfi2, ia, ie, maxsto
      double precision DFIL(nfi),DSTOR(MAXSTO),DFL,DFH
   10 DFL=DSTOR(IA+NFI2-1)*DFIL(NFI2)
      DO 20 I=1,NFI2-1
   20 DFL=DFL+(DSTOR(IA+I-1)+DSTOR(IE+1-I))*DFIL(I)
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
C     Compute highpass filtered observations by subtracting the        !
C     lowpass filtered observation from the original observation:      !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   30 DFH=DSTOR(IA+NFI2-1)-DFL   
      RETURN
      END
c 
c ======================================================================
C
      SUBROUTINE ETLFIN(CFILE,mfi,DDTSEC,NFI,nfi2,DLF,
     1 CFILT,IERR)
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c
c based on a copy of subroutine ETFILT from analyze.f
c
c filters are generally understood to be zero-phase
c the set of coefficients is symmetric with respect to the last
c coefficient in the file
c they are complemented to a symmetric set by
c
c     DO 200 I=1,NFI2
c     READ(IUN15,17008) KFI,DLF(I)
c     DLF(NFI-I+1)=DLF(I)
c 200 CONTINUE
c
c discard sampling interval consistency check and return 
c design sampling interval through DDTSEC
C                                                                      !
C     Routine ETLFIN, version 1996.08.26 Fortran 90.                   !
C                                                                      !
C     The routine reads an symmetrical nor-recursive numerical lowpass !
C     filter (FIR) to be used within Earth tide analysis program       !
C     ANALYZE.                                                         !
C                                                                      !
C     Input parameter description:                                     !
C     ----------------------------                                     !
C                                                                      !
C     CFILE:       Path name for numewrical lowpass filter. The        !
C                  file CFILEN will be opened by the execution of      !
C                  routine ETLFIN and the numerical lowpass filter     !
C                  will be read from this file.                        !
c     mfi:         size of array dlf
C     DDTSEC:      Sampling interval in sec.                           !
C                                                                      ! 
C     Output parameter description:                                    !
C     -----------------------------                                    !
C                                                                      !
C     NFI:         Length of filter in hours (number of filter         !
C                  coefficients).                                      !
C     DLF:         Array of lowpass filter coefficients (1...2001).    !
C     CFILT:       Name of the filter (CHARACTER*12).                  !
C     IERR:        Error code. IERR=1, if an error occured during the  !
C                  execution of routine ETLFIN.                        !
C                                                                      !
C     Numerical accuracy:                                              !
C     -------------------                                              !
C                                                                      !
C     The routine has been tested on an IBM-PC using DOUBLE PRECISION  !
C     (i.e. 15 digits) for all non-integer variables.                  !
C                                                                      !
C     Routine creation:  1988.02.18 by Hans-Georg Wenzel,              !
C                        Black Forest Observatory,                     !
C                        Universitaet Karlsruhe,                       !
C                        Englerstr. 7,                                 !
C                        D-76128 KARLSRUHE 1,                          !
C                        Germany.                                      !
C                        Tel: 0049-721-6082307,                        !
C                        FAX: 0049-721-694552.                         !
C                        e-mail: wenzel@gik.bau-verm.uni-karlsruhe.de  !
C     Last modification: 1996.08.07 by Hans-Georg Wenzel.              !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      IMPLICIT DOUBLE PRECISION (D)
      IMPLICIT INTEGER (I-N)
      integer IUN15
      parameter(iun15=10)
      integer mfi, nfi, nfi2
      DOUBLE PRECISION DLF(mfi)
      CHARACTER CFILT*12,CFILE*(*)
      CHARACTER CTEXT(8)*10,CENDT*10
      DATA CENDT/'C*********'/
C DBG
      print *,'read filter from file ',CFILE(1:INDEX(CFILE,' ')-1)
      OPEN(UNIT=IUN15,FILE=CFILE,err=300,STATUS='OLD')
  100 READ(IUN15,17003) (CTEXT(I),I=1,8)
      IF(CTEXT(1).NE.CENDT) GOTO 100
      READ(IUN15,17005) CFILT
      READ(IUN15,17006) NFI
      READ(IUN15,17006) NFI2
      READ(IUN15,17007) DDTS
      READ(IUN15,17007) DDTRESAMP
      print 17001, CFILT,NFI,DDTS
      IF(NFI.GT.mfi) THEN
         print *,' nfi=',nfi,'  mfi=',mfi
         print 17009
         IERR=1 
         RETURN
      ENDIF
CC disable consistency check
      DDTSEC=DDTS
CC       IF(DABS(DDTS-DDTSEC).GT.1.D0) THEN
CC          print 17010
CC          IERR=1
CC          RETURN
CC       ENDIF
      DO 200 I=1,NFI2
      READ(IUN15,17008) KFI,DLF(I)
      DLF(NFI-I+1)=DLF(I)
  200 CONTINUE
      IERR=0
      CLOSE(UNIT=IUN15)
      print 17002
      return
  300 print *,'ERROR: opening file'
      ierr=1
      RETURN
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
C     Format statements:                                               !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
17001 FORMAT(6x,'Routine ETLFIN, version 1996.08.07.'/
     1 6X,'Used numerical filter               is ',A12/
     2 6X,'Length of the used numerical filter is ',I10/
     3 6X,'Sampling interval                   is ',F10.3,' s'/)
17002 FORMAT(6x,'Routine ETLFIN finished the execution.'/)
17003 FORMAT(8A10)
17004 FORMAT(6X,7A10)
17005 FORMAT(10X,A10)
17006 FORMAT(10X,I10)
17007 FORMAT(10X,F10.3)
17008 FORMAT(I8,D24.15)
17009 FORMAT(/
     1 6X,'***Error in routine ETLFIN.'/
     2 6X,'***Filter length exceeds maximum of 2001.'/
     3 6X,'***Routine ETLFIN stops the execution for this project.')
17010 FORMAT(/
     1 6X,'***Error in routine ETLFIN.'/
     2 6X,'***Numerical lowpass filter is not constructed for the ',
     3 'sampling interval.'/
     4 6X,'***Routine ETLFIN stops the execution for this project.')
      END
c
c ----- END OF FIRspec.f ----- 
