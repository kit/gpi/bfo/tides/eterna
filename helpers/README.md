Helpers for Eterna
==================

This directory provides auxiliary programs and shell scripts.

**Table of contents**

* [List of files](#list-of-files)
  * [`processIERS.sh`](#processierssh)
  * [`mainharmonics.awk`](#mainharmonicsawk)
  * [`etfiltconv.awk`](#etfiltconvawk)
  * [`FIRspec.f`, `FIRspec.gpt.tpl`](#firspecf-firspecgpttpl)
  * [`etfilter.f`, `etfilter.gpt.tpl`](#etfilterf-etfiltergpttpl)
  * [`Makefile`](#makefile)
* [Usage](#usage)
  * [Update `etpolut1.dat`](#update-etpolut1dat)
    * [References to the Eterna manual](#references-to-the-eterna-manual)
  * [Display Eterna filter factors](#display-eterna-filter-factors)
    * [`etfiltconv.awk`](#etfiltconvawk-1)
    * [`FIRspec.f`, `FIRspec.gpt.tpl`](#firspecf-firspecgpttpl-1)
    * [`etfilter.f`, `etfilter.gpt.tpl`](#etfilterf-etfiltergpttpl-1)

List of files
-------------
The following files are provided:

### `processIERS.sh`
  This shell script is used to update `etpolut1.dat`.
  See [INSTALL (EOP update procedure)](../INSTALL.md#eop-update-procedure) 
  and section [Update `etpolut1.dat`](#update-etpolut1dat)
  for information on how to use this shell script.

### `mainharmonics.awk`
  This awk-script scans tidal catalog files and reports harmonics, 
  for which the amplitude exceeds a given threshold and/or which are
  labelled by a name. See the comment header of the file for usage 
  instructions.

### `etfiltconv.awk`
  This awk-scripts reads `*.nlf` filter files as provided by Eterna
  and converts them to a full sequence of FIR filter coefficients.
  See the comment header of the file for usage instructions and the
  examples and rules provided in `Makefile`.
  See also section [`etfiltconv.awk`](#etfiltconvawk-1).

  *Needed external programs:*
  `gawk`, `foutra`, and `stuplox` are needed. 
  The latter two are provided by 
  [Seitosh](https://gitlab.kit.edu/kit/thomas.forbriger/seitosh).

### `FIRspec.f`, `FIRspec.gpt.tpl`
  Display filter factors of Eterna's definition of FIR filters in `*.nlf` files.
  See section [`FIRspec.f`, `FIRspec.gpt.tpl`](#firspecf-firspecgpttpl-1).

  *Needed external programs:*
  A Fortran compiler, `cat`, `sed`, and `gnuplot` are needed.

### `etfilter.f`, `etfilter.gpt.tpl`
  Display filter factors of Eterna's definition of FIR filters in `*.nlf` files.
  See section [`etfilter.f`, `etfilter.gpt.tpl`](#etfilterf-etfiltergpttpl-1).

  *Needed external programs:*
  A Fortran compiler, `cat`, `sed`, and `gnuplot` are needed.

### `Makefile`
  The file provides various `make` rules to create graphical diagrams
  of the filter response defined in Eterna's `*.nlf` files. See the comments
  in the file for usage instruction and see section [Display Eterna filter factors](#display-eterna-filter-factors) in [Usage](#usage).

Usage
-----

### Update `etpolut1.dat`
  This shell script is used to update `etpolut1.dat`.
  See [INSTALL (EOP update procedure)](../INSTALL.md#eop-update-procedure) 
  for further information on how to use this shell script.

  To create polar motion data and DUT1 from scratch from 1962 to now run

    processIERS.sh download

  To process an existing IERS file to polar motion and DUT1 run

    processIERS.sh filename

  where 'filename' is the name of the data file downloaded from IERS.

#### References to the Eterna manual
  Please note that the download URL in the manual is out-dated. Current data is
  provided by IERS at http://hpiers.obspm.fr/eoppc/eop/eopc04

  Copied from eterna34.hlp (line 134):

    Compared to previous versions, we have included into the ETERNA  earth
    tide data processing package the most accurate tidal  potential  cata-
    logue of Hartmann and Wenzel (1995a,b), and the DUT1 correction due to
    the Earth's variable rotation. The DUT1 values provided by the  Inter-
    ational Earth  Rotation  Service  (IERS)  are  used  in  the  relevant 
    programs. These upgrades enable the  computation  of  synthetic  earth 
    tide signals with a model accuracy better than 1  ngal  (1 ngal = 0.01
    nm/s**2). 

  Copied from eterna34.hlp (line 506):

    eopc04.96        17.200  13.08.96  IERS ftp file, ASCII
    etddt.dat        11.316  23.03.96  TDT-UTC, ASCII
    etpolut1.dat    603.131  13.08.96  IERS polar motion and DUT1, ASCII
    etpolut1.uft    718.560  13.08.96  IERS polar motion and DUT1, binary

  Copied from eterna34.hlp (line 1883):

    Section 9: Description of program IERS
    --------------------------------------
   
    The program IERS.FOR is  supplied  in  order  to  transform  an  earth
    orientation parameter file, as e.g.  "eopc04.96"  available  from  the
    International Earth Rotation Service (IERS) via ftp  into  the  format
    necessary for the ETERNA package. The source code of  the  program  is 
    provided in file "\eterna33\sourcef\iers.for",  the executable program
    is provided in file "\eterna33\bin\iers.exe". The program  is  written 
    in standard Fortran 90. 
    
    The IERS earth rotation parameters can be transferred from IERS 
    Central Bureau at Paris via FTP:
   
    adress:     149.238.2.21
    login:      anonymous
    password:   your e-mail adress
    directory:  cd iers cd old
    files:      eopc04.xx where xx stands for the year.
   
    The program IERS assumes  the  input  data  in  IERS  format  on  file 
    "eopc04.96", and provides the output on  the  print  file  "iers.prn". 
    The data from this output have to be added at the  end  of  the  ASCII 
    file  "etpolut1.dat"  and  the  unformatted  file   "etpolut1.uft"  in
    directory "eterna33\commdat" has to be deleted in order to update  the
    pole coordinates and DUT1 corrections  for  the  ETERNA  package.  The 
    program IERS has to be updated and re-compiled after 1996 or when  a
    new leap second has been introduced in UTC. When a new leap second has
    been introduced, you should also update file "etddt.dat" in directory
    "eterna33\commdat".

### Display Eterna filter factors

#### `etfiltconv.awk`
  Convert Eterna `*.nlf` FIR filter files to ASCII time series data ready to
  be used with `stuplox` and `foutra`. 
  `stuplox` and `foutra` are available as part of
  [Seitosh](https://gitlab.kit.edu/kit/thomas.forbriger/seitosh).

  Appropriate make rules are provided in the Makefile. To study the
  properties of filter `n1h1h007.nlf` for example, run the following commands:

  To display the FIR coefficients:

    make n1h1h007.psp

  To display the frequency response:
    
    make n1h1h007.S.psp

  To display the frequency response on a frequency axis in cycles per day:
    
    make n1h1h007.Scpd.psp

  Notice that Eterna applies band-pass filters by subtracting a band-reject
  filtered time series from the original data. Similarly a high-pass is
  accomplished by subtracting a low-pass filtered time series from the
  original.

  **NOTICE:** This only displays the magnitude of frequency response
  filter factors, not their sign!


#### `FIRspec.f`, `FIRspec.gpt.tpl`
  Display filter factors of Eterna's definition of FIR filters 
  in `*.nlf` files.
  The factors are displayed on a frequency scale given as a fraction of Nyquist
  frequency.
  Factors are displayed on linear and logarithmic scales.

  Needed external programs:
  A Fortran compiler, `cat`, `sed`, and `gnuplot` are needed.

  The `Makefile` provides rules to run these programs.
  See the comments in the `Makefile`.

  For example run

    make n20s1m01.FIR.pdf
    
  to create a diagram for filter file `n20s1m01.nlf`. 
  To select the number of displayed points run

    make n20s1m01.FIR.pdf NFIR=200
    
  To obtain a compilation of all decimation filters run

    make DEC.pdf

#### `etfilter.f`, `etfilter.gpt.tpl`
  Display filter factors of Eterna's definition of FIR filters in `*.nlf`
  files.
  The factors are displayed on a frequency scale in units of cpd (cycles per day).

  Needed external programs:
  A Fortran compiler, `cat`, `sed`, and `gnuplot` are needed.

  The `Makefile` provides rules to run these programs.
  See the comments in the `Makefile`.

  For example run

    make n60m60m2.resp.pdf
    
  to create a diagram for filter file `n60m60m2.nlf`. 
  To select the frequency range and number of points run

    make n60m60m2.resp.pdf FMIN=0.5 FMAX=3. NF=200
