#!/bin/sh
# this is <processIERS.sh>
# 
# Copyright (c) 2012, 2019 by Thomas Forbriger (BFO Schiltach) 
# 
# shell script to process IERS data
#
# ----
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ----
# 
# REVISIONS and CHANGES 
#    19/09/2012   V1.0   Thomas Forbriger
#    28/06/2019   V1.1   adjust to current source URL
#                        add etpolut1.dat header
#    02/06/2022   V1.2   - use generic download URL
#                        - stop after printing usage
#                        - adjust the file header for etpolut1.dat
# 
# ============================================================================
#
VERSION=2022-06-02
echo $(basename $0) version $VERSION

if test $# -lt 1
then
  echo "Create of polar motion file for use with eterna"
  echo process IERS time series
  echo
  echo usage:
  echo $0 \<filename\>
  echo or
  echo $0 download
  echo
  echo where:
  echo filename   is the name of an EOP data file obtained from IERS
  echo download   requests to download EOP data from IERS first
  echo
  echo results will be written to etpolut1.dat
  exit 2
fi

IERS=$(which iers 2>/dev/null)

if test \! -x "$IERS"
then
  echo iers executable is missing
  exit 2
fi

EOPFILE=
if test z$1 == z'download'
then
  SRCURL="https://hpiers.obspm.fr/eoppc/eop/eopc04/"
  EOPFILE=eopc04.62-now
  src=${SRCURL}${EOPFILE}
  wget -N $src
else
  SRCURL=file://$(pwd)
  EOPFILE=$1
fi

echo $EOPFILE > iers.ini

$IERS | tee iers.stdout
cat > etpolut1.dat <<HERE
FILE     : ETPOLUT1.DAT
STATUS   : $(date +'%Y.%m.%d')
CONTENTS : Pole coordinates and earth rotation,
           one day sampling interval, given at 0 hours UTC.

           This file is created by $(basename $0)

           The pole coordinates, UT1-UTC and TAI-UT1 have been taken from 
           file $EOPFILE, transferred from IERS via INTERNET 
           at $(date +'%Y.%m.%d') and transformed to current format using
           program iers.f

           source loaction of $EOPFILE: 
           $SRCURL

           International Earth Rotation Service
           Central Bureau (IERS/CB)
           Observatoire de Paris
           61, avenue de l'Observatoire
           F-75014 PARIS
           France
           e-mail: iers@obspm.fr

Date     Time    MJD          x         y       UT1-UTC   TAI-UT1               
                             / "       / "      / s       / s                   

C****************************************************************************** 
HERE
cat iers.out >> etpolut1.dat
echo "99999999" >> etpolut1.dat

# ----- END OF processIERS.sh ----- 
