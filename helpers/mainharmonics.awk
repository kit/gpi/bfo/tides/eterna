#!/bin/gawk
# this is <mainharmonics.awk>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
# 
# extract main harmonics from tidal catalog
#
# ----
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ----
#
# run
#
# gawk -f ./mainharmonics.awk ../eterna34/commdat/tamurahw.dat
# 
# or (setting a threshold)
#
# gawk -v TH=1.e9 -f ./mainharmonics.awk ../eterna34/commdat/tamurahw.dat
# 
# or (setting a threshold and selecting by threshold only)
#
# gawk -v TH=1.e9 -v JT=1 \
#   -f ./mainharmonics.awk ../eterna34/commdat/tamurahw.dat
# 
# REVISIONS and CHANGES 
#    23/10/2019   V1.0   Thomas Forbriger
# 
# ============================================================================
#
function headline() {
  printf "Filename: %s\n", FILENAME;
  printf "Threshold: %7.4g 10**-10 m**2/s**2\n", threshold;
  if (justbythreshold)
  {
    printf "select just by thershold\n\n";
  }
  else
  {
    printf "select by thershold and name being given\n\n";
  }
  printf "%5s %10s %22s %5s\n", "name", "frequency", "amplitude", "name"; 
  printf "%5s %10s %22s %5s\n\n", "", "/ cpd", "/ 10**-10 m**2/s**2", ""; 
  }
BEGIN { 
  hot=0; 
  standard_threshold=4.e7;
  threshold=TH;
  justbythreshold=JT;
  if (threshold < standard_threshold) { threshold=standard_threshold; }
}
/^C\*\*\*/ { hot=1; headline(); next; }
/^9999/ { next; }
{ 
  if (hot) {
    f=substr($0,45,12);
    C0=substr($0,57,12);
    S0=substr($0,69,12);
    name=substr($0,102);
    A=sqrt(C0*C0+S0*S0);

    if (justbythreshold>0) 
    {
      if (A>=threshold) 
      {
        printf "%5s %10.6f % 22.1f %5s\n",
               name, 24.*f/360., A, name;
      }
    }
    else
    {
      if ((match(name, "[^ ]")) || (A>=threshold)) 
      {
        printf "%5s %10.6f % 22.1f %5s\n",
               name, 24.*f/360., A, name;
      }
    }
  }
}
# ----- END OF mainharmonics.awk ----- 
