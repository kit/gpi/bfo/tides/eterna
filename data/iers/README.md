Data example for `iers`
=======================

Files
-----
Files downloaded from https://hpiers.obspm.fr/eoppc/eop/eopc04/:

    eopc04.20
    eopc04.21
    eopc04.22
    
Configuration file for `iers`:

    iers.ini
    
The configuration file just lists the names of the input file.

Test run
--------
Run

    iers
    
in this subdirectory to see how the program works.

Further comments
----------------
There is no need to run this program manually. 
Usually you would run the program through [processIERS.sh](../../helpers/processIERS.sh).
Details are discussed in [INSTALL](../../INSTALL.md).