#!/bin/sh
# this is <runanalyze.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2022 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# run analyze
# 
# REVISIONS and CHANGES 
#    02/06/2022   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
VERSION=2022-06-02
echo example program to run analyze and compare with the benchmark data
echo version: $VERSION

# provide default.ini if not present
if test \! -r default.ini
then
  ln -sfv ../default.ini .
  echo created symbolic link for default.ini
  echo run shell script again
  exit 2
fi

echo
echo run analyze
../../bin/analyze

echo
echo compare results with benchmark data
for d in SG056L14.far SG056L14.par SG056L14.prn SG056L14.res analyze.prn
do
  echo ${d}:
  diff ${d} benchmark/${d} | tee diff_${d}
done

echo
echo differences are only expected for entries indicating the program version
echo and entries indicating the CPU time used

echo
echo see
ls -l diff_*

# ----- END OF runanalyze.sh ----- 
