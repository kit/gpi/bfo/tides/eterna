Example case for `analyze`
==========================

This directory contains data, configuration files, 
and benchmark results for `analyze`.


Files and subdirectory
----------------------
`SG056L14.dat`  
Gravity time series data from SG056 sensor 1 (lower) at BFO

`SG056L14.ini`  
Configuration file for the analysis of the gravity time series.

`project`  
Project file which lists the one and only project name (SG056L14)
in this directory.

`benchmark/SG056L14.far`  
`benchmark/SG056L14.par`  
`benchmark/SG056L14.prn`  
`benchmark/SG056L14.res`  
`benchmark/analyze.prn`  
Benchmark results obtained with the original code of Eterna 3.40
(with the 3rd party code originally used in Eterna) for the
configuration given in `SG056L14.ini`. The comparison with this
benchmark data confirms the correct computation done with the
replacement code, at least for this data example.

`runanalyze.sh`  
A shell script to conveniently run this example.

Usage
-----
Run the shell script

    runanalyze.sh
    
If no `default.ini` is persent, the shell script will
first create a symbolic link. Then re-run the script.

The shell-script runs `analyze` with the configuration 
present in this directory. The output of analyze is 
compared with benchmark results, which are provided in
subdirectory `benchmark`. The differences to the 
benchmark files are computed and stored in files `diff_*`.
Differences are only expected for lines indicating
the program version or the CPU computation time.
