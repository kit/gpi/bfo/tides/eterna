#!/bin/sh
# this is <runpredict.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2022 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# run predict
# 
# REVISIONS and CHANGES 
#    02/06/2022   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
VERSION=2022-06-02
echo example program to run predict and compare with the benchmark data
echo version: $VERSION

echo
echo run predict
../../bin/predict

echo
echo compare results with benchmark data
for d in SG56Lsyn.prd SG56Lsyn.prn
do
  echo ${d}:
  diff ${d} benchmark/${d} | tee diff_${d}
done

echo
echo differences are only expected for entries indicating the program version
echo and entries indicating the CPU time used

echo
echo see
ls -l diff_*

# ----- END OF runpredict.sh ----- 
