Example case for `predict`
==========================

This directory contains configuration files 
and benchmark results for `predict`.


Files and subdirectory
----------------------
`SG56Lsyn.ini`  
Configuration file to compute a synthetic gravity time series
based on a tidal model for BFO.

`project`  
Project file which lists the one and only project name (SG056Lsyn)
in this directory.

`benchmark/SG56Lsyn.prd`  
`benchmark/SG56Lsyn.prn`  
Benchmark results obtained with the original code of Eterna 3.40
(with the 3rd party code originally used in Eterna) for the
configuration given in `SG056Lsyn.ini`. The comparison with this
benchmark data confirms the correct computation done with the
replacement code, at least for this data example.

`runpredict.sh`  
A shell script to conveniently run this example.

Usage
-----
Run the shell script

    runpredict.sh
    
The shell-script runs `predict` with the configuration 
present in this directory. The output of predict is 
compared with benchmark results, which are provided in
subdirectory `benchmark`. The differences to the 
benchmark files are computed and stored in files `diff_*`.
Differences are only expected for lines indicating
the program version or the CPU computation time.