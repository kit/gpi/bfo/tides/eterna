Data examples
=============

This directory contains example data, example configuration files, and benchmark results.

[analyze](analyze/README.md):   
Data example for `analyze`.

[predict](predict/README.md):   
Data example for `predict`.

[iers](iers/README.md):   
Data example for `iers`.

`default.ini`:   
Default configuration file to be used with `analyze` and `predict`.
See the comments in the file for further information.