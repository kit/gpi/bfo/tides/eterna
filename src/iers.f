      PROGRAM IERS
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
C                                                                      !
C     Program IERS, version 2019.05.29 Fortran 90.                     !
C                                                                      !
C     Most recent modification: 2019.07.03                             !
C                                                                      !
C Report all modifications under REVISIONS and CHANGES (see below).    !
C Update the data statement for CLCH accordingly.                      !
C                                                                      ! 
C -------------------------------------------------------------------- !
C iers - Transform IERS earth rotation pole parameters.                !
C                                                                      ! 
C Copyright (C) 1972, 1996, 1997 by Hans-Georg Wenzel                  ! 
C                                                                      !
C Revisions and corrections:                                           !
C Copyright (C) 2002 by Bernard Ducarme                                !
C Copyright (C) 2011, 2019 by Hartmut Wziontek                         !
C                                                                      !
C iers is provided as part of Eterna, a collection of programs for     !
C tidal analysis and prediction.                                       !
C                                                                      !
C iers is free software: you can redistribute it and/or modify         !
C it under the terms of the GNU General Public License as published by !
C the Free Software Foundation, either version 3 of the License, or    !
C (at your option) any later version.                                  !
C                                                                      !
C iers is distributed in the hope that it will be useful,              !
C but WITHOUT ANY WARRANTY; without even the implied warranty of       !
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        !
C GNU General Public License for more details.                         !
C                                                                      !
C You should have received a copy of the GNU General Public License    !
C along with this program.                                             !
C If not, see <https://www.gnu.org/licenses/>                          !
C -------------------------------------------------------------------- !
C                                                                      !
C REVISIONS and CHANGES  (date format: DD/MM/YYYY)                     !
C   02/09/1997    Hans-Georg Wenzel                                    !
C   19/06/2002    Bernard Ducarme                                      !
C   12/04/2011    Hartmut Wziontek (BKG)                               !
C                 new EOPC-format, reduced output to stdout            !
C   29/05/2019    Hartmut Wziontek (BKG)                               !
C   03/07/2019    Thomas Forbriger (KIT, BFO)                          !
C                                                                      !
C -------------------------------------------------------------------- !
C                                                                      !
C     This file has 244 records.                                       ! 
C                                                                      !
C     Transforms IERS earth rotation pole parameters.                  !
C     Input is IERS format as transferred via www from IERS Paris:     !
C                                                                      !
C               http://hpiers.obspm.fr                                 !
C                                                                      !
C     Output is ETERNA format for file etpolutc.dat on file iers.out.  !
C     Print file is iers.prn.                                          ! 
C                                                                      !
C     Input file names are read from formatted file iers.ini.          !
C                                                                      !
C     Program creation:  1996.01.01 by Hans-Georg Wenzel,              !
C                        Black Forest Observatory,                     !
C                        Universitaet Karlsruhe,                       !
C                        Englerstr. 7,                                 !
C                        D-76128 KARLSRUHE 1,                          !
C                        Germany.                                      !
C                        Tel: 0049-721-6082307,                        !
C                        FAX: 0049-721-694552.                         !
C                        e-mail: wenzel@gik.bau-verm.uni-karlsruhe.de  !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      IMPLICIT DOUBLE PRECISION (D)
      CHARACTER CTEXT(8)*10,CFILE*256,CFILE2*256
      CHARACTER CLCH*8
      DATA IUN10/10/,IUN11/11/,IUN12/12/,IUN16/16/
      DATA CLCH/'20190703'/
      OPEN(UNIT=IUN12,FILE='iers.out',FORM='FORMATTED')
      OPEN(UNIT=IUN16,FILE='iers.prn',FORM='FORMATTED')
      WRITE(*,17000) CLCH
      WRITE(IUN16,17000) CLCH
      IRESET=1
      ISCREEN=1
      CALL GEOEXT(IUN16,IRESET,ISCREEN,DEXTIM,DEXTOT)
      OPEN(UNIT=IUN10,FILE='iers.ini',FORM='FORMATTED')
      DTAIOLD=0.D0
      INIT=1
  100 READ(IUN10,17003,END=1000) CFILE
      CFILE2=CFILE(ILTRIM(CFILE):LENGTH(CFILE))
      OPEN(UNIT=IUN11,FILE=CFILE2,FORM='FORMATTED')
      DO 110 J=1,14
      READ(IUN11,17001) (CTEXT(I),I=1,8)
  110 CONTINUE
C      READ(IUN11,17008) IYEAR
C      READ(IUN11,17001) (CTEXT(I),I=1,8)
  200 CONTINUE
      READ(IUN11,17005,END=500)IYEAR,IMON,IDAY,IJUL,DX,DY,DUT1
  220 CONTINUE
      IDAT=10000*IYEAR+100*IMON+IDAY
      DMJUL=DBLE(IJUL)
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
C     Take care of leap seconds:                                       !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
      DLEAPS=10.0D0
      IF(IDAT.GE.19720701) DLEAPS=11.D0
      IF(IDAT.GE.19730101) DLEAPS=12.D0
      IF(IDAT.GE.19740101) DLEAPS=13.D0
      IF(IDAT.GE.19750101) DLEAPS=14.D0
      IF(IDAT.GE.19760101) DLEAPS=15.D0
      IF(IDAT.GE.19770101) DLEAPS=16.D0
      IF(IDAT.GE.19780101) DLEAPS=17.D0
      IF(IDAT.GE.19790101) DLEAPS=18.D0
      IF(IDAT.GE.19800101) DLEAPS=19.D0
      IF(IDAT.GE.19810701) DLEAPS=20.D0
      IF(IDAT.GE.19820701) DLEAPS=21.D0
      IF(IDAT.GE.19830701) DLEAPS=22.D0
      IF(IDAT.GE.19850701) DLEAPS=23.D0
      IF(IDAT.GE.19880101) DLEAPS=24.D0
      IF(IDAT.GE.19900101) DLEAPS=25.D0
      IF(IDAT.GE.19910101) DLEAPS=26.D0
      IF(IDAT.GE.19920701) DLEAPS=27.D0
      IF(IDAT.GE.19930701) DLEAPS=28.D0
      IF(IDAT.GE.19940701) DLEAPS=29.D0
      IF(IDAT.GE.19960101) DLEAPS=30.D0
      IF(IDAT.GE.19970701) DLEAPS=31.D0
      IF(IDAT.GE.19990101) DLEAPS=32.D0
      IF(IDAT.GE.20060101) DLEAPS=33.D0
      IF(IDAT.GE.20090101) DLEAPS=34.D0
      IF(IDAT.GE.20120701) DLEAPS=35.D0
      IF(IDAT.GE.20150701) DLEAPS=36.D0
      IF(IDAT.GE.20170101) DLEAPS=37.D0

      DTAI=DLEAPS-DUT1
C
      IF(INIT.EQ.1) THEN
      WRITE(IUN12,17007) IDAT,DMJUL,DX,DY,DUT1,DTAI
      WRITE(IUN16,17007) IDAT,DMJUL,DX,DY,DUT1,DTAI
         INIT=0
         DTAIOLD=DTAI
         GOTO 200
      ENDIF
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
C     Check for leap second:                                           !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  300 DDT=DTAI-DTAIOLD
      IF(DDT.GT.0.5D0) THEN
         DLEAPS=DLEAPS-1.D0
         WRITE(IUN16,17009) IDAT,DLEAPS
      ENDIF
      IF(DDT.LT.-0.5D0) THEN
         DLEAPS=DLEAPS+1.D0
         WRITE(IUN16,17009) IDAT,DLEAPS
      ENDIF
      DTAI=DLEAPS-DUT1
      DDT=DTAI-DTAIOLD
      IF(DABS(DDT).GT.0.5D0) GOTO 300
      WRITE(IUN12,17007) IDAT,DMJUL,DX,DY,DUT1,DTAI
      WRITE(IUN16,17007) IDAT,DMJUL,DX,DY,DUT1,DTAI
      DTAIOLD=DTAI
      GOTO 200
  500 GOTO 100
 1000 CONTINUE
      WRITE(IUN16,17010)
      CALL GEOEXT(IUN16,IRESET,ISCREEN,DEXTIM,DEXTOT)
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
C     Format statements:                                               !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
17000 FORMAT(' ######################################################'/
     1       ' #                                                    #'/
     2       ' #    Program IERS.FOR, version 1997.09.02 Fortran90. #'/
     z       ' #            last modification ',A8,  '              #'/
     3       ' #                                                    #'/
     4       ' #    Transformation of IERS EOP97C04-files into      #'/
     5       ' #    ETERNA format.                                  #'/
     6       ' #                                                    #'/
     7       ' ######################################################'/)
17001 FORMAT(8A10)
17002 FORMAT(1X,7A10,A9)
17003 FORMAT(A,I5,F6.2)
17004 FORMAT(//' data input file is: ',A/)
C17005 FORMAT(2X,I4,2X,A3,I4,2X,I5,2F9.6,F10.7,2X,F10.7,2X,2F9.6)
C17005 FORMAT(2X,A3,I4,I7,2F9.5,F10.6)
C NEW IERS FORMAT 05 C04 / 2010-03-18 HWz
C17005 FORMAT(3(I4),I7,2(F11.6),2(F12.7),2(F11.6),2(F11.6),2(F11.7),
C     1 2F12.6)
17005 FORMAT(3(I4),I7,2(F11.6),F12.7)
17006 FORMAT(1X,I8,1X,'000000',F10.3,2F10.5,2F10.6)
17007 FORMAT(I8,1X,'000000',F10.3,2F10.5,2F10.6)
17008 FORMAT(10X,I5)
17009 FORMAT(' *** Leap second at: ',I8,' DLEAPS=',F10.3)
17010 FORMAT(' ######################################################'/
     1       ' #                                                    #'/
     2       ' #    Program IERS.FOR finished execution.            #'/
     3       ' #                                                    #'/
     4       ' ######################################################'/)
      END
C
      SUBROUTINE GEOEXT(IUN16,IRESET,ISCREEN,DEXTIM,DEXTOT)
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
C                                                                      !
C     Routine GEOEXT, version 1997.08.22 Fortran 90.                   !
C                                                                      !
C     The routine GEOEXT computes the actual job time and writes       !
C     the actual execution time on printer output unit IUN6.           !
C     For the first call of routine GEOEXT, the actual jobtime will    !
C     be computed (in secs since midnight) and stored. For the next    !
C     call(s) of routine GEOEXT, the actual jobtime will be computed   !
C     and the execution time (actual jobtime minus jobtime of the      !
C     first call of routine GEOEXT) will be printed.                   !
C                                                                      !
C     Input parameter description:                                     !
C     ----------------------------                                     !
C                                                                      !
C     IUN16:       formatted printer unit.                             !
C     IRESET:      DEXTIM will be resetted, if IRESET=1.               !
C     ISCREEN:     Execution time will also be written on the screen   !
C                  of the PC.                                          !    
C                                                                      !
C     Output parameter description:                                    !
C     -----------------------------                                    !
C                                                                      !
C     DEXTIM:      actual jobtime in seconds (time elapsed from the    !
C                  last call of routine GEOEXT with IRESET=1 to the    !
C                  actual call of routine GEOEXT), double precision.   !
C     DEXTOT:      total jobtime in seconds (time elapsed from the     !
C                  first call of routine GEOEXT), double precision.    !  
C                                                                      !
C     Used routines:                                                   !
C     --------------                                                   !
C                                                                      !
C     SYSTEM-CLOCK                                                     !
C                                                                      !
C     Execution time:                                                  !
C     ---------------                                                  !
C                                                                      !
C     0.17 msec per call of GEOEXT with ISCREEN=0 on a PENTIUM 100 MHZ !
C     PC.                                                              !
C                                                                      !
C     Program creation:  1979.08.30 by Hans-Georg Wenzel,              !
C                        Black Forest Observatory,                     !
C                        Universitaet Karlsruhe,                       !
C                        Englerstr. 7,                                 !
C                        D-76128 KARLSRUHE,                            !
C                        Germany.                                      !
C                        Tel.: 0721-6082301.                           !
C                        FAX:  0721-694552.                            !
C                        e-mail: wenzel@gik.bau-verm.uni-karlsruhe.de  !
C     Last Modification: 1997.08.22 by Hans-Georg Wenzel.              !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      IMPLICIT DOUBLE PRECISION (D)
C MSFOR:      INTEGER*2 IH,IM,IS,IS100
      DATA IFIRST/1/
      SAVE DTIME1
      IF(IRESET.NE.1) GOTO 6003
C MSFOR:      CALL GETTIM(IH,IM,IS,IS100)
C MSFOR:      DTIME1=DBLE(IS+IM*60+IH*3600)+0.01*FLOAT(IS100)
C LAHEY 90:
      CALL SYSTEM_CLOCK(IC,ICR)
      DTIME1=DBLE(IC)/DBLE(ICR)
C UNIX:      DTIME1=DBLE(SECNDS(RDUMMY))
      WRITE(IUN16,17001)
      IF(ISCREEN.EQ.1) WRITE(*,17001)
      DEXTIM=0.D0
      DEXTOT=0.D0
      IF(IFIRST.EQ.1) THEN
        DTIME0=DTIME1
        IFIRST=0
      ENDIF 
      IRESET=0
      RETURN
 6003 CONTINUE
C MSFOR:      CALL GETTIM(IH,IM,IS,IS100)
C MSFOR:      DTIME2=DBLE(IS+IM*60+IH*3600)+0.01*FLOAT(IS100)
C LAHEY:
      CALL SYSTEM_CLOCK(IC,ICR)
      DTIME2=DBLE(IC)/DBLE(ICR)
C UNIX: DTIME2=DBLE(SECNDS(RDUMMY))   
      DEXTIM=DTIME2-DTIME1
      DEXTOT=DTIME2-DTIME0
      WRITE(IUN16,17002) DEXTIM
      IF(ISCREEN.EQ.1) WRITE(*,17002) DEXTIM
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
C     Format statements:                                               !
C!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
17001 FORMAT(6x,'First call of routine GEOEXT, version 1997.08.22.')
17002 FORMAT(/6x,'Routine GEOEXT. Execution time=',F10.3,' sec'/)
      RETURN
      END

      INTEGER FUNCTION ILTRIM(STRING) 
      CHARACTER*(*) STRING 
      ILTRIM=1
      DO 10, I=1,LEN(STRING)
         IF(STRING(I:I) .NE. ' ') THEN
            ILTRIM=I
            GO TO 20
         END IF
 10   CONTINUE 
 20   RETURN
      END


      INTEGER FUNCTION LENGTH(STRING) 
C Returns length of string ignoring trailing blanks 
      CHARACTER*(*) STRING 
      DO 15, I = LEN(STRING), 1, -1 
         IF(STRING(I:I) .NE. ' ') GO TO 20 
15    CONTINUE 
20    LENGTH = I 
      END
