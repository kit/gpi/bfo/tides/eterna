# this is <Makefile>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
# 
# compile and install Eterna source code
# --------------------------------------
#
# targets:
#   install: 	compiles and installs binary executables
#   doc: 		converts manual and source code to PDF and Postscript
#   etbase: 	creates appropriate $HOME/.etbase file
#                 if the file does not yet exist
#   all: 		combines targets install, doc, and etbase
#
# make variables:
#   INSTALLDIR:	defines the location of binary files 
#                 default: local subdirectory bin
#   PDFDIR:		defines location of PDF and Postscript documents
#                 default: local subdirectory pdfdoc
#   FC:		defines the name of the Fortran compiler
#   			default: gfortran
#
# example:
#   run
#     
#     make all INSTALLDIR=$HOME/bin PDFDIR=$HOME/Documents
#
#   to install all binary executable in $HOME/bin, all PDF and Postscript
#   documents in $HOME/Documents, and install $HOME/.etbase if it does not yet
#   exist
#
# required programs (to create all targets):
#   - the Fortran compiler specified in make variable FC
#     preferably gfortran
#   - install
#   - a2ps
#   - ps2pdf
#   - printf
#   - cat
#   - pwd
#   - mkdir
# 
# REVISIONS and CHANGES 
#    01/07/2019   V1.0   Thomas Forbriger
# 
# ============================================================================
#
# set installation directory to local bin directory if not yet set
INSTALLDIR?=bin

# set installation directory for PDF and Postscript versions of documentation
PDFDIR?=pdfdoc

# set gfortran as Fortran compiler if not yet set
FC?=gfortran
 
# names of programs to be compiled and installed
PROGRAMS=analyze predict iers

# names of Postscript and PDF files to be created in directory doc
DOC=eterna34.ps eterna34.pdf \
    $(addsuffix .ps,$(PROGRAMS)) \
    $(addsuffix .pdf,$(PROGRAMS)) \
    $(addsuffix .index.ps,$(PROGRAMS)) \
    $(addsuffix .index.pdf,$(PROGRAMS)) 

# location of etbase file
ETBASE=$(HOME)/.etbase

#----------------------------------------------------------------------
# instalaltion targets
#
.PHONY: all
all: install doc etbase

# install binary executables from source code
.PHONY: install
install: $(addprefix $(INSTALLDIR)/,$(PROGRAMS))
$(INSTALLDIR)/%: src/%
	install -vpD $< $@

# install Postscript and PDF versions of ASCII files
.PHONY: doc
doc: $(addprefix $(PDFDIR)/,$(DOC))

# install etbase file
.PHONY: etbase
etbase: $(ETBASE)
	@printf "\nInstallation path set in $<:\n"
	cat $<

#----------------------------------------------------------------------
# support editing
# ---------------
LOCALFILES=CHANGELOG CONTRIBUTORS COPYING LICENSE Makefile README.md
flist: $(LOCALFILES) \
  $(wildcard src/*.f */README */COPYING)
	echo $^ | tr ' ' '\n' | sort > $@

.PHONY: edit
edit: flist; vim $<

# remove intermediate files
.PHONY: clean
clean:  
	-find . -name \*.bak | xargs --no-run-if-empty /bin/rm -v
	-/bin/rm -vf flist *.ps *.pdf *.o *.xxx
	-/bin/rm -vf */*.o */*.xxx
	-/bin/rm -vf $(addprefix src/,$(PROGRAMS))

# remove installed files
.PHONY: allclean
allclean: clean
	-/bin/rm -vf $(addprefix $(PDFDIR)/,$(DOC))
	-/bin/rm -vf $(addprefix $(INSTALLDIR)/,$(PROGRAMS))

# ============================================================================
# compile software
# ----------------

FFLAGS += -ff2c -Wall -ffixed-line-length-0 -fno-backslash $(FLAGS)

$(addprefix src/,$(PROGRAMS)): src/%: src/%.f
	$(FC) $(FFLAGS) -o $@ $^

# ============================================================================
# etbase
# ------
$(ETBASE): ; pwd > $@

# ============================================================================
# Postscript and PDF files of manual and source code
# --------------------------------------------------
#
# convert Postscript to PDF
%.pdf: %.ps; ps2pdf -sPAPERSIZE=a4 $< $@
#
#----------------------------------------------------------------------
# Eterna manual
# -------------
MANUAL=doc/eterna34.hlp
$(PDFDIR)/eterna34.ps: $(MANUAL)
	mkdir -pv $(dir $@)
	export LANG=C; \
		a2ps --encoding=utf-8 -o $@ -C -A sheet -1 \
		  --borders=no --margin=36 --sides=duplex $< 

#----------------------------------------------------------------------
# source code listing
# -------------------
LINESPERPAGE=100
$(PDFDIR)/%.ps: src/%.f
	mkdir -pv $(dir name $@)
	export LANG=C; \
		a2ps --encoding=utf-8 -o $@ -C -A sheet -1 --borders=no \
		  --margin=36 -L $(LINESPERPAGE) --sides=duplex $< 

# index to functions
$(PDFDIR)/%.index.xxx: src/%.f
	mkdir -pv $(dir name $@)
	printf "Functions and subroutines:\n" > $@
	-egrep -n '(^ +SUBROUTINE|FUNCTION)' $< >> $@
	printf "Common blocks:\n" >> $@
	-egrep -n '^ +COMMON' $< >> $@
	printf "Common blocks in functions and subroutines:\n" >> $@
	-egrep -n '(^ +(SUBROUTINE|COMMON)|FUNCTION)' $< >> $@
$(PDFDIR)/%.index.ps: $(PDFDIR)/%.index.xxx
	export LANG=C; \
		a2ps --encoding=utf-8  -o $@ -A sheet -1 --borders=no \
		  --margin=36 --sides=duplex \
		  --center-title="Index to functions, subroutines and common blocks in $(patsubst $(PDFDIR)/%.index.xxx,src/%.f,$<)" $< 
	/bin/rm -fv $<

# ----- END OF Makefile ----- 
