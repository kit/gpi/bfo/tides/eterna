Eterna - installation and usage instructions
============================================

**Table of contents**

* [Manual](#manual)
* [Installation](#installation)
* [Directory structure](#directory-structure)
  * [Directories provided by Eterna](#directories-provided-by-eterna)
    * [`commdat`](#commdat)
    * [`data`](#data)
    * [`doc`](#doc)
    * [`helpers`](#helpers)
    * [`src`](#src)
  * [Created directories](#created-directories)
    * [`bin`](#bin)
    * [`pdfdoc`](#pdfdoc)
  * [Working directory](#working-directory)
* [Maintenance](#maintenance)
  * [Programs and files](#programs-and-files)
    * [`src/iers.f`](#srciersf)
    * [`commdat/etddt.dat`](#commdatetddtdat)
    * [`commdat/etpolut1.dat`](#commdatetpolut1dat)
  * [EOP update procedure](#eop-update-procedure)

Manual
------
Eterna 3.40 comes with a detailed manual, which is provided in file 
`doc/eterna340.hlp`. This is the original text as provided by Hans-Georg
Wenzel in 1997. General usage instructions, in particular the definition of
data formats and configuration files as well as the workings of the major
programs analyze and predict are still valid. 

The layout of the directory structure, however, is different for the current
Linux/UNIX version.

Installation
------------
Installation is supported by the `Makefile` in the root directory of Eterna.
See the comments in the `Makefile` for available targets and Linux/UNIX tools
required for compilation and installation. If all are in place the easiest way
to install Eterna is by running

    make all INSTALLDIR=<path>

where `<path>` should be replaced by an appropriate path name to the desired
installation directory. For convenience this path should be included in the
shell's `PATH` variable.

Directory structure
-------------------

### Directories provided by Eterna 
#### `commdat`
  This subdirectory contains common data files like catalogues of harmonic
  development of the tide generating potential, FIR filter coefficients, a
  table of leap seconds, and earth orientation and rotation parameters. The
  location of this directory is specified either by `/etc/etbase` (system
  wide) or by `$HOME/.etbase` (per user). The etbase files contain a single
  string, which is the path name of the parent directory of commdat. If not
  yet present, `$HOME/.etbase` can be created by running

    make etbase

#### `data`
  This subdirectory is prepared to contain a toy example of gravity data and
  configuration files for analyze and predict. See the file [README](data/README.md) in the
  subdirectory for further details.

#### `doc`
  This subdirectory contains the official Eterna manual and other
  documentation coming along with the package.

#### `helpers`
  This subdirectory contains auxiliary programs and shell scripts. See the
  file [README](helpers/README.md) in the subdirectory for further details.

#### `src`
  This subdirectory contains the source code.

### Created directories
#### `bin`
  This subdirectory contains binary executables and will be created by

    make install

  if no other value is set for make variable `INSTALLDIR`.

#### `pdfdoc`
  This subdirectory contains PDF and Postscript versions of the Eterna manual
  and source code and will be created by

    make doc

  if no other value is set for make variable `PDFDIR`.

### Working directory
  The common data files are to be placed in `commdat`. Their location will be
  resolved by analyze and predict through `/etc/etbase` or `$HOME/.etbase`,
  respectively. Any directory can be used to process or simulate tidal data be
  running analyze or predict. In this working directory the file `project`,
  which specifies the base name of all other files, together with these
  appropriately named data files and configuration files must be provided. See
  the Eterna manual (`doc/eterna34.hlp`) for details. The file `default.ini`
  is read from the current working directory as well.

Maintenance
-----------
Eterna requires Earth orientation and Earth rotation parameters in order to
produce accurate results. Since Earth rotation cannot exactly predicted
a-priori, parameters as derived from observations must be provided. It is the
users responsibility to keep these data up-to-date. There are three locations
where data must be maintained.

The appropriate source for these data is the *International Earth Rotation and
reference systems Service* (IERS, http://hpiers.obspm.fr/eop-pc/index.php).

### Programs and files
#### `src/iers.f`
  The program `src/iers.f` contains a table of leap-seconds in a list of
  if-conditionals. Leap seconds are announced by *Bulletin C* of the IERS
  (https://hpiers.obspm.fr/iers/bul/bulc/). A table of leap-seonds is provided
  for download at http://hpiers.obspm.fr/iers/bul/bulc/Leap_Second.dat

#### `commdat/etddt.dat`
  This file provides a table to interpolate DDT = difference ET - UTC resp.
  TDT - UTC in seconds after 1820.5, where ET is *Ephemeris Time*, TDT is
  *Terrestrial Dynamical Time*, and UTC is *Universal Time Coordinated*. See
  comments in the file for further details. For each leap second two
  additional entries must be made in the file.

  Data may be updated manually based on the table of leap seconds provided at
  http://hpiers.obspm.fr/iers/bul/bulc/Leap_Second.dat
  Alternatively a recent copy of `etddt.dat` is provided by IGETS at 
  http://igets.u-strasbg.fr/Documents/etddt.dat
  
#### `commdat/etpolut1.dat`
  This file provides pole coordinates and Earth rotation data. It must be
  updated from data provided by the IERS at
  https://hpiers.obspm.fr/eoppc/eop/eopc04/
  The auxiliary shell script `helpers/processIERS.sh` can download and process
  these data in order to create an up-to-date version of `etpolut1.dat`. Just
  change your working directory to `commdat` and run

    ../helpers/processIERS.sh download

  Make sure to update iers.f (see above) before running this procedure.

  Alternatively a recent copy of `etpolut1.dat` is provided by IGETS at 
  http://igets.u-strasbg.fr/Documents/etpolut1.dat

  `helpers/README` provides some references to the Eterna manual regarding
  Earth orientation and rotation parameters.

### EOP update procedure
1) Check http://hpiers.obspm.fr/iers/bul/bulc/Leap_Second.dat and compare with leap seconds incorporated in `src/iers.f`

       ...
       IF(IDAT.GE.20120701) DLEAPS=35.D0
       IF(IDAT.GE.20150701) DLEAPS=36.D0
       IF(IDAT.GE.20170101) DLEAPS=37.D0
       ...

   Add additional lines, if necessary and recompile `iers`.

2) Check `commdat/etddt.dat` and make sure, it contains an entry for each leap second. The last DDT value only appears once, while all others appear twice:

       ...
       2015.50000 2457204.500000    68.184
       2016.99999 2457754.499999    68.184
       2017.00000 2457754.500000    69.184

   This is different from what is presented in http://igets.u-strasbg.fr/Documents/etpolut1.dat but matches the file originally published by Hans-Georg Wenzel together with Eterna 3.40.
   
3) Update `commdat/etpolut1.dat` by running

       ../helpers/processIERS.sh download

   in subdirectory `commdat`.
